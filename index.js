var express = require('express'),
  app = express(),
  http = require('http'),
  httpServer = http.Server(app);


app.use(express.static(__dirname + '/static'));

app.get('/castlabs-basic-usage', function(req, res) {
  res.sendFile(__dirname + '/castlabs-basic-usage.html');
});

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});
app.listen(3000);