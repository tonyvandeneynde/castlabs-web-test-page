/**
 * See this class as a connector between the castlabs prestoplay player (https://castlabs.com/products/prestoplay-browser/)
 * and the app, it implements a player interface that every third-party
 * player should implement.
 */

// *************** TODO LIST ********************
// TODO: Check if encryption works on safari with non-native HLS prioritized!
// **********************************************
var castlabsPlayer = new function (){
  const LOGGING = false
  let instance

  class CastlabsPlayer {
    // ------------------------------------
    // Constructor & Initialization
    // ------------------------------------
    constructor () {
      if (!instance) {
        console.log('init player')
        this.callbacksToRegister = {
          'clpp-video-track-changed': this.logState,
          'clpp-selected-tech': this.logState,
          'clpp-video-track-changed-auto': this.logState
        }
        this.initialized = false
        this.startMuted = false
        this.startPaused = false
        instance = this
      }
      return instance
    }
  
    init (widevineDrm, playreadyDrm, fairplayDrm, fairplayCertificateUrl, licenseKey) {
      //const licenseKey = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpbHYiOmZhbHNlLCJ0eXBlIjoiV2ViIiwidXJscyI6WyI6Ly9hMS5uZXQiXSwia2lkIjoxODMzfQ.GNR82I-k_i7G2i0GkrX7VQhQIzFBALUEXA0lHSpLqQi_xT4Ql2kTbvlBDk8wGdFbLKNiJDVVnWn8pDxi73HsQpUQZjfLDYkCIKhI4g8pswb27hlfrHZu26AWzFmqSKL-MXOTuFsqvaZQzDSydUfTFK0SROJhpGtDM3fJhwmwrsZPnb62yVqKQzWj82pxhIhM5RFWtuh8JR3h2Ahc8F7xj1HxdZfUovDJ-0QE_VUDIrS4tuAOgDOUXNlumSbd-Df4sptU5-BDljrCFIcv99zEUh5qHV6nWz8bWyum_li9r62qTZm8v-qDVLZ8mlV1POTYKurOXrefy-FOVYtqKBNidA'
      clpp.log.setLogLevel(clpp.log.Level.DEBUG)
  
      let playerConfig = {
        stopPlayOnErrors: true,
        license: licenseKey,
        autoplay: false,
        chromecast: {
          appId: 'D80D5506'
        },
        controls: true,
        techOrder: [
          clpp.statics.techs.CAST,
          clpp.statics.techs.HTML5_MSE_DASH,
          clpp.statics.techs.HTML5_MSE_SMOOTH,
          clpp.statics.techs.HTML5_MSE_HLS,
          clpp.statics.techs.HTML5_HLS,
          clpp.statics.techs.HTML5
        ],
        drm: this.getDRMConfig(widevineDrm, playreadyDrm, fairplayDrm, fairplayCertificateUrl),
        //drmtoday: this.getDRMTodayConfig()
        viewerId: "a1_at_showtime_308"
      }
  
      console.log('PLAYER CONFIG', JSON.stringify(playerConfig))
      console.log('playerConfig:', playerConfig)
  
      this.player = clpp.init('#video', playerConfig)
      _.forOwn(this.callbacksToRegister, (value, key) => this.player.on(key, value))
      this.initialized = true
    }
  
    // ------------------------------------
    // "Public" Methods (called on the player instance & directly accessed outside this file)
    // ------------------------------------
  
    open (url, drmProtected, callback) {
      // Setting the preferred languages (e.g. channel languages and first- and second languages from Settings)
      // during loading of the player because if we set them afterwards, the wrong language is used for a split-second.
      //const preferredAudioLanguage = this.getPreferredAudioLanguages(audioLanguage)
      //const preferredTextLanguage = this.getPreferredSubtitleLanguages(subtitleLanguage)
      // const maxBandwidth = this.getMaxBandwidth(channelId)
      console.log('open')
      let options = {}
      options = { abr: {restrictions: {}},
                  preferredAudioLanguage: ['de', 'deu', 'ger', 'en', 'eng', 'eng', 0],
                  preferredTextLanguage: ['none', 'none', 'none', 'none', 'none', 'none'],
                  startTime:300
                  }
      this.player.load([{ src: url, drmProtected: drmProtected}], options)
        .then(() => {
          callback()
          this.onPlayerLoadSuccess()
        })
        .catch((e)=>{'playback failed', e})
    }
  
    play () {
      this.player.play()
    }
  
    pause () {
      this.player.pause()
    }
  
    ended () {
      return this.player.ended()
    }
  
    state () {
      return this.player.state()
    }
  
    dispose (force = false) {
      if (force || this.isPlayerAttached()) {
        return this.player.dispose()
      }
    }
  
    // Returns the state of the CastlabsPlayer
    isInitialized () {
      return this.initialized
    }
  
    removeSubtitles () {
      this.player.selectText(undefined)
    }
  
    isPlayerAttached () {
      return this.player.isPlayerAttached()
    }
  
    getAudioTracks () {
      return this.player.getTracksByType('audio')
    }
  
    getVideoTracks () {
      return this.player.getTracksByType('video')
    }
  
    getSubtitleTracks () {
      return this.player.getTracksByType('text')
    }
  
    getActiveTrackByType (type) {
      return this.player.getActiveTrackByType(type)
    }
  
    selectAudio (id) {
      return new Promise((resolve) => {
        this.player.selectAudio(id)
        resolve()
      })
    }
  
    selectSubtitles (id) {
      return new Promise((resolve) => {
        this.player.on('clpp-text-track-changed', (data) => { resolve(data) })
        this.player.selectText(id)
      })
    }
  
    // ------------------------------------
    // "Private" Methods (helpers for functions inside this file)
    // ------------------------------------
  
    // Callback registration from the playerMiddleware
    on (event, cb) {
      this.player ? this.player.one(event, cb) : this.callbacksToRegister[event] = cb
    }
  
    // Event registration + callbacks when video_js is initialized
    onPlayerLoadSuccess () {
      // Correct state when player has started. ORDER IS IMPORTANT!
      if (this.player.isPlayerAttached()) {
        console.log('onplayerloadsuccess 1')
        this.player.play(); // Start player (MUST be called for player to start playing on clicks!)
        console.log('onplayerloadsuccess 2')
        (this.startMuted || muted) ? this.player.muted(true) : this.player.volume(volume) // Startmuted or else re-initialize previous audio state
      }
      console.log('onplayerloadsuccess 3')
    }
  
    onPlayerLoadFail () {
      this.callbacksToRegister.playerLoadFailed()
    }
  
    logState () {
      // console.log(this, e)
    }

    getDRMTodayConfig () {
      return {
        type: 'DRM_TODAY',
        merchant: 'wind',
        environment: 'PRODUCTION',
        userId: '743676873',
        sessionId: 'e2f05e45-c3db-46bb-9b74-f792d5644d67',
        assetId: 'ch002',
      }
    }
  
    getDRMConfig (widevineUrl, playreadyUrl, fairplayUrl, fairplayCertURL) {
      let drmPlayerConfig = {}
      let widevineLicenseURL = widevineUrl
      let playreadyLicenseURL = playreadyUrl
      let fairplayLicenseURL = fairplayUrl
      let fairplayCertificateURL = fairplayCertURL
  
      if (widevineLicenseURL) {
        drmPlayerConfig['com.widevine.alpha'] = {
          licenseUrl: widevineLicenseURL,
          modifiers: vcasConfig.widevine.modifiers
        }
      }
  
      if (playreadyLicenseURL) {
        drmPlayerConfig['com.microsoft.playready'] = {
          licenseUrl: playreadyLicenseURL,
          modifiers: vcasConfig.playready.modifiers
        }
      }
  
      if (fairplayLicenseURL && fairplayCertificateURL) {
        drmPlayerConfig['com.apple.fps.1_0'] = {
          licenseUrl: fairplayLicenseURL,
          certificateUrl: fairplayCertificateURL,
          modifiers: vcasConfig.fairplay.modifiers
        }
      }
      return drmPlayerConfig
    }
  }
  return new CastlabsPlayer()
}
