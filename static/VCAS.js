var vcasConfig = new function () {
  getFingerPrint = function () { return '176684c07f30563786dc44270e0a64ccsdafsfsdf' }

  const drmData = {
    drm: {
      widevine: {
        licenseServerUrl: 'https://widevine.showtime.a1.net:8063'
      },
      fairplay: {
        licenseServerUrl: 'https://fairplay.juliet.a1.net:8064/fpsa/v1.0/',
        certificateUrl: 'https://sdsevocdn.romeo.a1.net/certs/fairplay.cer',
        siteId: '12345'
      },
      playready: {
        licenseServerUrl: 'https://playready.showtime.a1.net:443/PlayReady/rightsmanager.asmx'
      }
    }
  }
  
  // WIDEVINE
  getWidevineLicenseServerURL = function (url) {
    const base64FingerPrint = Base64.encode(getFingerPrint())
    const licenseServerUrl = url
    return licenseServerUrl ? licenseServerUrl + '?deviceId=' + base64FingerPrint : undefined
  }
  
  // FAIRPLAY
  const getFairplayLicenseServerURL = (url) => {
    const base64FingerPrint = Base64.encode(getFingerPrint())
    const licenseServerUrl = url
    return licenseServerUrl ? licenseServerUrl + '?deviceId=' + base64FingerPrint : undefined
  }
  
  const getFairplayCertificateURL = (url) => {
    const certificateUrl = url
    return certificateUrl
  }
  
  // PLAYREADY
  const getPlayreadyLicenseServerURL = (url) => {
    const base64FingerPrint = Base64.encode(getFingerPrint())
    const licenseServerUrl = url
    return licenseServerUrl ? licenseServerUrl + '?deviceId=' + base64FingerPrint : undefined
  }
  
  return {
    widevine: {
      getLicenseServerURL: (deviceId) => getWidevineLicenseServerURL(deviceId),
      modifiers: {
        licenseRequest: (config, response) => { },
        licenseResponse: (config, response) => { }
      }
    },
    fairplay: {
      getLicenseServerURL: (deviceId) => getFairplayLicenseServerURL(deviceId),
      getCertificateURL: () => getFairplayCertificateURL(),
      modifiers: {
        licenseRequest: function (config, request) {
          request.body = 'spc=' + encodeURIComponent(btoa(String.fromCharCode.apply(null, new Uint8Array(request.body))));
          request.headers['Content-Type'] = 'application/x-www-form-urlencoded';
          console.log('license request', request)
      },
        licenseResponse: (config, response) => {
          /*
            The response to a license request is a Base64 encoded JSON object containing the Content Key Context (CKC). For additional information, refer to the Apple developer document: FairPlay Streaming Overview.
            {
              "ckc" :  [Base64 encoded <ckc message>]
            }
          */
          const data = clpp.utils.ab2str(response.data);
          const jsonData = JSON.parse(data);
          const modified = jsonData.ckc;
          response.data = clpp.utils.str2ab(modified);
  
          console.log('licence response', response)
        },
        extractContentId: (config, initData) => {
          const uint16array = new Uint16Array(initData.buffer)
          const contentId = String.fromCharCode.apply(null, uint16array)
          const link = document.createElement('a')
          link.href = contentId
          let result = contentId.split('://')[1]
          console.log('extractContentId', result)
          return result
        }
      }
    },
    playready: {
      getLicenseServerURL: (deviceId) => getPlayreadyLicenseServerURL(deviceId),
      modifiers: {
        licenseRequest: (config, response) => { },
        licenseResponse: (config, response) => { }
      }
    }
  }
}

